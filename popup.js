// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';
/*
let changeColor = document.getElementById('changeColor');

chrome.storage.sync.get('color', function(data) {
  changeColor.style.backgroundColor = data.color;
  changeColor.setAttribute('value', data.color);
});

changeColor.onclick = function(element) {
  let color = element.target.value;
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.executeScript(
        tabs[0].id,
        {code: 'document.body.style.backgroundColor = "' + color + '";'});
  });
};
*/
$('#txt-content').on('input', function(e) {
  let textVal = $(this).val();
  let word = textVal.substring(textVal.lastIndexOf(" ") + 1);
  if (word.length >= 2) {
    let last2Char = word.substring(word.length -2, word.length);
    console.log('Last 2 char: ' + last2Char);
    let replacedChar = getReplacedChar(last2Char);
    console.log('Replaced char: ' + replacedChar);
    if (replacedChar != last2Char) {
      let newTextVal = textVal.substring(0, textVal.length -2) + replacedChar;
      console.log('New text: ' + newTextVal);
      $(this).val(newTextVal);
    }
}
})

function getReplacedChar(last2Char) {
  let replacedChar = charMaps[last2Char];
  if (replacedChar) {
    return replacedChar;
  }
  return last2Char;
}



let charMaps = {
  'as': 'á', 'af': 'à', 'ar': 'ả', 'ax': 'ã', 'aj': 'ạ',
  'aw': 'ă',
  'ăs': 'ắ', 'ăf': 'ằ', 'ăr': 'ẳ', 'ăx': 'ẵ', 'ăj': 'ặ', 
  'aa': 'â',
  'âs': 'ấ', 'âf' : 'ầ', 'âr': 'ẩ', 'âx': 'ẫ', 'âj': 'ậ',
  'ee': 'ê',
  'es': 'é', 'ef': 'è', 'er': 'ẻ', 'ex': 'ẽ', 'ej': 'ẹ',
  'ês': 'ế', 'êf': 'ề', 'êr': 'ể', 'êx': 'ễ', 'êj': 'ệ',
  'is': 'í', 'if': 'ì', 'ir': 'ỉ', 'ix': 'ĩ', 'ij': 'ị',
  'os': 'ó', 'of': 'ò', 'or': 'ỏ', 'ox': 'õ', 'oj': 'ọ',
  'ow': 'ơ',
  'ơs': 'ớ', 'ơf': 'ờ', 'ơr': 'ở', 'ơx': 'ỡ', 'ơj': 'ợ',
  'oo': 'ô',
  'ôs': 'ố', 'ôf': 'ồ', 'ôr': 'ổ', 'ôx': 'ỗ', 'ôj': 'ộ',
  'us': 'ú', 'uf': 'ù', 'ur': 'ủ', 'ux': 'ũ', 'uj': 'ụ',
  'uw': 'ư',
  'ưs': 'ứ', 'ưf': 'ừ', 'ưr': 'ử', 'ưx': 'ữ', 'ưj': 'ự',
  'dd': 'đ',
}